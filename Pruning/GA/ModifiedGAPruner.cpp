/*
 * ModifiedGAPruner.cpp
 *
 *  Created on: Feb 2, 2011
 *      Author: rgreen
 */

#include "ModifiedGAPruner.h"

ModifiedGAPruner::ModifiedGAPruner(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
				: GAPruner(popSize, generations, o, g, l, p, ul) {
}
ModifiedGAPruner::ModifiedGAPruner(int popSize, int generations, double mut, double cross, Classifier* o, std::vector<Generator> g, std::vector<Line> l,
		double p, bool ul)
				: GAPruner(popSize, generations, mut, cross, o, g, l, p, ul) {
}

ModifiedGAPruner::~ModifiedGAPruner() {

}

void ModifiedGAPruner::Prune(MTRand& mt){
	timer.startTimer();
	initTime	= 0,
	fitnessTime = 0,
	selectTime	= 0,
	crossTime	= 0,
	mutTime		= 0;

	pop.resize(Np, 0.0);
	
	#ifdef _MSC_VER
		bestFitness = -std::numeric_limits<double>::infinity();
	#else
		bestFitness = -INFINITY;
	#endif

	initPopulation(mt);

	totalIterations = 0;
	while(!isConverged()){
		evaluateFitness();
		mutate(mt);
		selectionAndCrossover(mt);
		totalIterations++;
	}

	timer.stopTimer();
	pruningTime = timer.getElapsedTime();
}
